
<?php

global $k_option;
$name_of_your_site = get_option('blogname');
$email_adress_reciever =  $k_option['contact']['email'];

$errorC = true;
if(isset($_POST['Send']))
{
        $host = bloginfo('wpurl') . 'wp-content/themes/newscast/' . 'send.php';
        include($host); 

}

$k_option['custom']['bodyclass'] = ""; //<-- Display Sidebar
// $k_option['custom']['bodyclass'] = "fullwidth"; //<-- Dont display Sidebar

?>


<?php global $is_ajax; $is_ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH']); if (!$is_ajax) get_header(); ?>
<?php $wptouch_settings = bnc_wptouch_get_settings(); ?>
 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 	<div class="post content" id="post-<?php the_ID(); ?>">
	 <div class="page">
		<div class="page-title-icon">		
			<?php bnc_the_page_icon(); ?>
		</div>
		<h2><?php the_title(); ?></h2>
	</div>
	      
<div class="clearer"></div>
  
    <div id="entry-<?php the_ID(); ?>" class="pageentry <?php echo $wptouch_settings['style-text-justify']; ?>">
        <?php if (!is_page('archives') || !is_page('links')) { 

	    the_content(); 
	}  
                                        if($preview_image)
                                        { 
                                                echo '<div class="entry-previewimage rounded preloading_background">';
                                                echo $preview_image;    
                                                echo '</div>';
                                        } 

                                        ?>
						<h1><?php echo $email_adress_reciever; ?></h1>
                                        <form action="" method="post" class="ajax_form">
					        <div style="padding: 0 10px;">
                                                <?php if (!isset($errorC) || $errorC == true){ ?><h3><span><?php _e('Send us mail','newscast'); ?></span></h3>
                                                
                                                <p class="<?php if (isset($the_nameclass)) echo $the_nameclass; ?>" ><input name="yourname" class="text_input is_empty" type="text" id="name" size="20" value='<?php if (isset($the_name)) echo $the_name?>'/><label for="name"><?php _e('Your Name','newscast'); ?>*</label>
                                                </p>
                                                <p class="<?php if (isset($the_emailclass)) echo $the_emailclass; ?>" ><input name="email" class="text_input is_email" type="text" id="email" size="20" value='<?php if (isset($the_email)) echo $the_email ?>' /><label for="email"><?php _e('E-Mail','newscast'); ?>*</label></p>
                                                <p><input name="website" class="text_input" type="text" id="website" size="20" value="<?php if (isset($the_website))  echo $the_website?>"/><label for="website">Website</label></p>
                                                <label for="message" class="blocklabel"><?php _e('Your Message','newscast'); ?>*</label>
                                                <p class="<?php if (isset($the_messageclass)) echo $the_messageclass; ?>"><textarea name="message" class="text_area is_empty" cols="40" rows="7" id="message" ><?php  if (isset($the_message)) echo $the_message ?></textarea></p>
                                                
                                                
                                                <p>
                                                
                                                <input type="hidden" id="myemail" name="myemail" value="<?php echo $email_adress_reciever; ?>" />
                                                <input type="hidden" id="myblogname" name="myblogname" value='<?php echo $name_of_your_site; ?>' />
                                                
                                                <input name="Send" type="submit" value="<?php _e('Send','newscast'); ?>" class="button" id="send" size="16"/></p>
                                                <?php } else { ?> 
                                                <p><h3><?php _e('Your message has been sent!','newscast'); ?> </h3> <?php _e('Thank you!','newscast'); ?> </p>
                                                
                                                <?php } ?>
                                                </div>

                                        </form>






<?php if (is_page('archives')) {
// If you have a page named 'Archives', the WP tag cloud will be displayed
?>
          </div>
	</div>

	<h3 class="result-text"><?php _e( "Tag Cloud", "wptouch" ); ?></h3>
		<div id="wptouch-tagcloud" class="post">
			<?php wp_tag_cloud('smallest=11&largest=18&unit=px&orderby=count&order=DESC'); ?>
		</div>
	</div>
</div>

	<h3 class="result-text"><?php _e( "Monthly Archives", "wptouch" ); ?></h3>
		<div id="wptouch-archives" class="post">
			<?php wp_get_archives(); // This will print out the default WordPress Monthly Archives Listing. ?> 
		</div>
		  
<?php } ?><!-- end if archives page-->
            
<?php if (is_page('photos')) {
// If you have a page named 'Photos', and the FlickrRSS activated and configured your photos will be displayed here.
// It will override other number of images settings and fetch 20 from the ID.
?>
	<?php if (function_exists('get_flickrRSS')) { ?>
		<div id="wptouch-flickr">
			<?php get_flickrRSS(20); ?>
		</div>
	<?php } ?>
<?php } ?><!-- end if photos page-->
		</div>
	</div>   
           		
<?php if (is_page('links')) {
// If you have a page named 'Links', a default listing of your Links will be displayed here.
?>
		</div>
	</div>          

		<div id="wptouch-links">
			<?php wp_list_bookmarks('title_li=&category_before=&category_after='); ?>
		</div>
<?php } ?><!-- end if links page-->    	
	
		<?php wp_link_pages( __('Pages in this article: ', 'wptouch'), '', 'number'); ?>

<!--If comments are enabled for pages in the WPtouch admin, and 'Allow Comments' is checked on a page-->
	<?php if (bnc_is_page_coms_enabled() && 'open' == $post->comment_status) : ?>
		<?php comments_template(); ?>
		<script type="text/javascript">
		jQuery(document).ready( function() {
		// Ajaxify '#commentform'
		var formoptions = { 
			beforeSubmit: function() {$wpt("#loading").fadeIn(400);},
			success:  function() {
				$wpt("#commentform").hide();
				$wpt("#loading").fadeOut(400);
				$wpt("#refresher").fadeIn(400);
				}, // end success 
			error:  function() {
				$wpt('#errors').show();
				$wpt("#loading").fadeOut(400);
				} //end error
			} 	//end options
		$wpt('#commentform').ajaxForm(formoptions);
		}); //End onReady
		</script>
  	<?php endif; ?>
<!--end comment status-->
    <?php endwhile; ?>	

<?php else : ?>

	<div class="result-text-footer">
		<?php wptouch_core_else_text(); ?>
	</div>

 <?php endif; ?>

<!-- If it's ajax, we're not bringing in footer.php -->
<?php global $is_ajax; if (!$is_ajax) 
get_footer();


