<?php

function enqueue_child_styles() {
	wp_enqueue_style( 'childe-style', get_bloginfo( 'stylesheet_url' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_child_styles', 99 );

function search_url_rewrite_rule() {
	if ( is_search() && !empty($_GET['s'])) {
		wp_redirect(home_url("/search/") . urlencode(get_query_var('s')));
		exit();
	}	
}
add_action('template_redirect', 'search_url_rewrite_rule');

if ( is_admin() ) {
?>
	<style type="text/css">
	#vwpc-container {display:none !important;}
	</style>
<?php
}

function acf_vwpc_render_post_section( $section, $the_query ) {
	$title = $section['title'];
	$super_title = $section['super-title'];
	$sub_title = $section['subtitle'];

	$thumbnail_style = $section['thumbnail_style'];
	$sidebar = $section['sidebar'];
	$show_pagination = $section['show_pagination'];

	// Render pagination
	$pagination_html = '';
	if ( '1' == $show_pagination ) {
		ob_start();
		global $wp_query;
		$main_query = $wp_query;
		$wp_query = $the_query;
		get_template_part( 'templates/pagination' );
		$wp_query = $main_query;
		$pagination_html = ob_get_clean();
	}

	// Render posts
	if ( $the_query->have_posts() ) : ?>

		<?php if ( 'large' == $thumbnail_style ) : ?>
			<?php if ( '0' == $sidebar ) : ?>
				<div class="col-sm-12">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row vw-isotope post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-4"><?php get_template_part( 'templates/post-box/large-thumbnail', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
			<?php else : ?>
				<div class="col-sm-7 col-md-8">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row vw-isotope post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-6"><?php get_template_part( 'templates/post-box/large-thumbnail', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
				<div class="col-sm-5 col-md-4">
					<aside class="sidebar-wrapper">
						<div class="sidebar-inner">
							<hr class="section-hr">
							<?php dynamic_sidebar( $sidebar ); ?>
						</div>
					</aside>
				</div>
			<?php endif; ?>

		<?php elseif ( 'poster' == $thumbnail_style ) : ?>
			<?php if ( '0' == $sidebar ) : ?>
				<div class="col-sm-12">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row vw-isotope post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-4"><?php get_template_part( 'templates/post-box/poster', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
			<?php else : ?>
				<div class="col-sm-7 col-md-8">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row vw-isotope post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-6"><?php get_template_part( 'templates/post-box/poster', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
				<div class="col-sm-5 col-md-4">
					<aside class="sidebar-wrapper">
						<div class="sidebar-inner">
							<hr class="section-hr">
							<?php dynamic_sidebar( $sidebar ); ?>
						</div>
					</aside>
				</div>
			<?php endif; ?>

		<?php elseif ( 'classic' == $thumbnail_style ) : ?>
			<?php if ( '0' == $sidebar ) : ?>
				<div class="col-sm-12">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-12"><?php get_template_part( 'templates/post-box/classic', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
			<?php else : ?>
				<div class="col-sm-7 col-md-8">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row post-box-list">
						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-12"><?php get_template_part( 'templates/post-box/classic', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
				<div class="col-sm-5 col-md-4">
					<aside class="sidebar-wrapper">
						<div class="sidebar-inner">
							<hr class="section-hr">
							<?php dynamic_sidebar( $sidebar ); ?>
						</div>
					</aside>
				</div>
			<?php endif; ?>

		<?php else : ?>
			<?php if ( '0' == $sidebar ) : ?>
				<div class="col-sm-12">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>
					
					<div class="row vw-isotope post-box-list">
						<?php foreach( range( 1, 3 ) as $i ): ?>
							<?php if ( $the_query->have_posts() ): $the_query->the_post(); ?>
								<div class="post-box-wrapper col-sm-4">
									<?php get_template_part( 'templates/post-box/large-thumbnail', get_post_format() ); ?>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>

						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-4"><?php get_template_part( 'templates/post-box/small-thumbnail', get_post_format() ); ?></div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>

			<?php else : ?>
				<div class="col-sm-7 col-md-8">
					<hr class="section-hr">
					<?php acf_vwpc_render_section_title( $title, $super_title, $sub_title ); ?>

					<div class="row vw-isotope post-box-list">
						<?php foreach( range( 1, 2 ) as $i ): ?>
							<?php if ( $the_query->have_posts() ): $the_query->the_post(); ?>
								<div class="post-box-wrapper col-sm-6">
									<?php get_template_part( 'templates/post-box/large-thumbnail', get_post_format() ); ?>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>

						<?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
							<div class="post-box-wrapper col-sm-6">
								<?php get_template_part( 'templates/post-box/small-thumbnail', get_post_format() ); ?>
							</div>
						<?php endwhile; ?>
					</div>

					<?php echo $pagination_html; ?>
				</div>
				<div class="col-sm-5 col-md-4">
					<aside class="sidebar-wrapper">
						<div class="sidebar-inner">
							<hr class="section-hr">
							<?php dynamic_sidebar( $sidebar ); ?>
						</div>
					</aside>
				</div>
			<?php endif; ?>
		<?php
		endif;
	endif;

	wp_reset_postdata();
}

function acf_vwpc_render_section_title( $title, $super_title='', $sub_title='' ) {
	if ( ! empty( $super_title ) ) $super_title = sprintf( '<span class="super-title">%s</span>', $super_title );

	if ( ! empty( $sub_title ) ) $sub_title = sprintf( '<p class="section-description">%s</p>', $sub_title );

	if ( ! empty( $title ) ) : ?>
		<h1 class="section-title title title-large">
			<?php echo $super_title . $title; ?>
		</h1>
		<?php echo $sub_title; ?>
	<?php endif;
}

function acf_vwpc_render_section_title_custom( $title, $super_title='', $sub_title='' ) {
	if ( ! empty( $super_title ) ) $super_title = sprintf( '<span class="super-title">%s</span>', $super_title );

	if ( ! empty( $sub_title ) ) $sub_title = sprintf( '<p class="section-description">%s</p>', $sub_title );

	if ( ! empty( $title ) ) : ?>
		<h1 class="section-title title">
			<?php echo $super_title . $title; ?>
		</h1>
		<?php echo $sub_title; ?>
	<?php endif;
}

function acf_vwpc_get_paged() {
	$paged = 1;
	if ( get_query_var('paged') ) $paged = get_query_var('paged');
	if ( get_query_var('page') ) $paged = get_query_var('page');

	return $paged;
}