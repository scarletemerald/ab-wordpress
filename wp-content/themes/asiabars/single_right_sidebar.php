<div id="page-wrapper" class="container">
	<div class="row">
		<div id="page-content" class="col-sm-8 col-md-9">
			<?php if (have_posts()) : ?>

				<?php while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="tags clearfix">
							<?php //vw_render_categories( '' ); ?>
						</div>

						<h1 class="section-title title title-large"><?php the_title(); ?></h1>						

						<hr class="hr-thin-bottom">

						<?php //if ( vw_get_option( 'blog_enable_sharebox' ) ) get_template_part( 'templates/social-share' ); ?>
						
						<?php get_template_part( 'templates/post-meta' ); ?>
						
						<?php get_template_part( 'templates/post-formats/format', get_post_format() ); ?>
						
						<div class="post-content clearfix">
							<?php the_content(); ?>
						</div>
						
						<!--<div class="post-media clearfix">
							<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
								</ol>
								
								<div class="carousel-inner">
									<div class="item active">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/image1-750x420.png" class="img-responsive" alt="image4">
									</div>
									
									<div class="item">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/image1-750x420.png" class="img-responsive" alt="image4">
									</div>
								</div>	
							</div>
						</div> -->
						
						<?php wp_link_pages( array(
								'before' => '<div class="post-page-links header-font">'.__( 'Pages:' ),
								'pagelink' => '<span>%</span>',
								'after' => '</div>',
							)
						 ); ?>

						<?php $posttags = get_the_tags();
						if ($posttags) : ?>
						<div class="post-tags clearfix">
							<span class="post-tags-title header-font"><?php //_e( 'Tags:', 'envirra' ) ?></span>
							<?php foreach( $posttags as $tag ) {
								//echo '<a class="label label-small label-light" href="'.get_tag_link($tag->term_id).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'envirra' ), $tag->name ) ) . '" rel="category">'.$tag->name.'</a>';
							} ?>
						</div>
						
						<hr class="hr-thin-bottom">
							<?php $key = 'venue_info';
							$themeta = get_post_meta($post->ID, $key, TRUE);
							if($themeta == '') 
							{
							?>
							<?php // echo "no venue info "; ?>
							
							<?PHP
							}
							else {	?>
							<div class="row venue-information">
							<div class="col-sm-6">			
							
								<h3 class="section-title">Venue Information</h3>
								<div class="information-block">
									<div class="row">
									<?php $key = 'address';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Address"; ?></div>
										<div class="col-sm-8 col-xs-6">
										<address>
											
										<?php  echo $themeta ; ?>
										</address>
										
										<?PHP }?>
										</div>
									</div>
									<div class="row">
									<?php $key = 'transport';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Transport"; ?></div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?></div> <?PHP }?> 
									</div>
									
									<div class="row">
									<?php $key = 'reservation';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Reservations"; ?></div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?></div> <?PHP }?> 
									</div>
									
									<div class="row">
									<?php $key = 'facebook_page';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Facebook Page"; ?></div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?> 
									</div>
									
									<div class="row">
									<?php $key = 'venue_website';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Website"; ?></div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?>
									</div>
									
									<div class="row">
									<?php $key = 'opening_hours';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6">Opening Hours</div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?>
									</div>
									<div class="row">
									<?php $key = 'promotion';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6"><?php echo "Promotion"; ?></div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?>
									</div>
									<div class="row">
									<?php $key = 'seats';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6">Seats</div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?> 
									</div>
									<div class="row">
									<?php $key = 'price_guide';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
										<div class="col-sm-4 col-xs-6">Price Guide</div>
										<div class="col-sm-8 col-xs-6"><?php  echo $themeta ; ?> </div> <?PHP }?>
									</div>
									
								</div>
							</div>
							
							<div class="col-sm-6">
								<h3 class="section-title">Map</h3>
								<?php $key = 'map';
										$themeta = get_post_meta($post->ID, $key, TRUE);
										if($themeta != '') {
										?>
								<?php  echo $themeta ; ?>  <?PHP }?>
							</div>
						</div>
					
						<?php } ?>
					
						
						<div class="bottom-social-share clearfix">
							<?php if ( vw_get_option( 'blog_enable_sharebox' ) ) get_template_part( 'templates/social-share' ); ?>
						</div>
						<?php  get_ssb('twitter=2&fblike=1&googleplus=3'); ?>
		
						
						<?php endif; ?>

					</article>
				<?php endwhile; ?>

				

				<?php if ( vw_get_option( 'blog_enable_related_post' ) ) get_template_part( 'templates/post-related' ); ?>
				

			<?php else : ?>

				<h2><?php _e('Sorry, no posts were found', 'envirra') ?></h2>

			<?php endif; ?>
		</div>

		<aside id="page-sidebar" class="sidebar-wrapper col-sm-4 col-md-3">
			<?php include('asiabars-sidebar.php'); ?>
		</aside>
	</div>
</div>