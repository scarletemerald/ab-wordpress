<?php
/*
Template Name: Page Composer
*/
get_header(); ?>

<?php if ( have_posts() ) : the_post(); ?>
	
<div id="page-wrapper" class="container">
	<div class="row">
		<div class="col-sm-8 col-md-9">
		
			<?php
				//vwpc_render();

				//echo print_r(get_field('field_56f60e82c25f2'), true);
				if(count(get_field('field_56f60e82c25f2') > 0) && get_field('field_56f60e82c25f2') != '' && get_field('field_56f60e82c25f2') != 0){

					foreach(get_field('field_56f60e82c25f2') as $section){

						echo '<div class="row vwpc-row">';

						switch($section['add_section']){
							case 'Featured Post Slider':
								$option_classes = '';
								$category = $section['featured_post_slider'][0]['category']->term_id;
								$show_posts = $section['featured_post_slider'][0]['show_posts'];
								$number = $section['featured_post_slider'][0]['number_of_featured_posts'];
								$sidebar = $section['featured_post_slider'][0]['sidebar'];
								$show_headline_posts = $section['featured_post_slider'][0]['show_headline_posts'];
								$has_sidebar = !empty($sidebar);

								if($has_sidebar) $option_classes .= ' has-sidebar ';

								$args = array(
									'post_type' => 'post',
									'orderby' => 'post_date',
									'ignore_sticky_posts' => true,
									'posts_per_page' => $number,
								);

								if($show_posts != 'all'){
									$args['meta_query'] = array(
										array(
											'key' => 'vw_post_featured', //Only posts that have review
											'value' => '1',
											'compare' => '=',
										)
									);
								}

								if($category > 0){
									$args['cat'] = $category;
								}

								query_posts($args);
			?>
								<div class="vwpc-section-featured_post_slider <?php echo $option_classes; ?>">
									<?php if ( $has_sidebar ) : ?>
									<div class="col-sm-7 col-md-8">
									<?php else : ?>
									<div class="col-sm-12">
									<?php endif; ?>
										<hr class="section-hr">
										<?php get_template_part( 'templates/post-box/post-slider' ); ?>
									
									<?php
									wp_reset_query();

									if ($show_headline_posts == 1) :
										$args['offset'] = $number;
										if ( $has_sidebar ) {
											$args['posts_per_page'] = 3;
											$column_classes = 'col-sm-6 col-md-4';
										} else {
											$args['posts_per_page'] = 4;
											$column_classes = 'col-sm-4 col-md-3';
										}
										$the_query = new WP_Query( $args );

										if ( $the_query->have_posts() ) :
										?>
											<div class="vwpc-section-featured_post_slider-headline row">
											<?php while ( $the_query->have_posts() ): $the_query->the_post();
												$hidden_class = '';
												if ( $the_query->current_post == $the_query->post_count-1 ) {
													$hidden_class = 'hidden-sm';
												}
											?>
												<div class="post-box-wrapper <?php echo $column_classes; ?> <?php echo $hidden_class; ?>"><?php get_template_part( 'templates/post-box/headline', get_post_format() ); ?></div>
											<?php endwhile; ?>
											</div>

										<?php endif;
										wp_reset_postdata();
									endif;
									?>
									</div>
									<?php if ( $has_sidebar ) : ?>
									<div class="col-sm-5 col-md-4">
										<aside class="sidebar-wrapper">
											<div class="sidebar-inner">
												<hr class="section-hr">
												<?php dynamic_sidebar( $sidebar ); ?>
											</div>
										</aside>
									</div>
									<?php endif; ?>
								</div>
			<?php
								break;

							case 'Latest Posts':
								$number = $section['latest_posts'][0]['number_of_posts'];
								$show_pagination = $section['latest_posts'][0]['show_pagination'];
								$paged = 1;
								if ( '1' == $show_pagination ) $paged = acf_vwpc_get_paged();

								$args = array(
									'post_type' => 'post',
									'orderby' => 'post_date',
									'ignore_sticky_posts' => true,
									'posts_per_page' => $number,
									'paged' => $paged,
									// 'meta_key' => '_thumbnail_id', //Only posts that have featured image
								);

								$the_query = new WP_Query( $args );

								echo '<div class="vwpc-section-latest">';
								acf_vwpc_render_post_section( $section['latest_posts'][0], $the_query );
								echo '</div>';
								break;

							case 'Latest Reviews':
								$number = $section['latest_reviews'][0]['number_of_posts'];
								$show_pagination = $section['latest_reviews'][0]['show_pagination'];
								$paged = 1;
								if ( '1' == $show_pagination ) $paged = acf_vwpc_get_paged();

								$the_query = new WP_Query( array(
									'post_type' => 'post',
									'orderby' => 'post_date',
									'ignore_sticky_posts' => true,
									'posts_per_page' => $number,
									'paged' => $paged,
									'meta_query' => array(
										array(
											'key' => 'vw_enable_review', //Only posts that have review
											'value' => '1',
											'compare' => '=',
										)
									),
								) );

								echo '<div class="vwpc-section-latest_reviews">';
								acf_vwpc_render_post_section( $section['latest_reviews'][0], $the_query );
								echo '</div>';
								break;

							case 'Latest By Category':
								$number = $section['latest_by_category'][0]['number_of_posts'];
								#echo $number;
								$show_pagination = $section['latest_by_category'][0]['show_pagination'];
								$posts_id = $section['latest_by_category'][0]['enter_the_post_id'];
								//echo 'postid=>'.$field_prefix.'_postid=>'.$posts_id;exit;
								$posts_idarray = explode(',',$posts_id);
								$paged = 1;
								if ( '1' == $show_pagination ) $paged = acf_vwpc_get_paged();
						        
								$args = array(
									'post_type' => 'post',
									'orderby' => 'post_date',
									'ignore_sticky_posts' => true,
									'posts_per_page' => $number,
									'paged' => $paged,
									'post__in' => $posts_idarray,
									
									//'meta_key' => '_thumbnail_id', //Only posts that have featured image
								);

								$category = $section['latest_by_category'][0]['category']->term_id;
								if(empty($category)){
									$category = 1;
								}
								
								
								if ( $category >= 1 ) {
									$args[ 'cat' ] = $category;
								}
								
								$the_query = new WP_Query( $args);
								//echo print_r($the_query, true);

								echo '<div class="vwpc-section-latest_category">';
								acf_vwpc_render_post_section( $section['latest_by_category'][0], $the_query );
								echo '</div>';
								break;

							case 'Latest By Format':
								$number = $section['latest_by_format'][0]['number_of_posts'];
								$show_pagination = $section['latest_by_format'][0]['show_pagination'];
								$paged = 1;
								if ( '1' == $show_pagination ) $paged = acf_vwpc_get_paged();

								$args = array(
									'post_type' => 'post',
									'orderby' => 'post_date',
									'ignore_sticky_posts' => true,
									'posts_per_page' => $number,
									'paged' => $paged,
									// 'meta_key' => '_thumbnail_id', //Only posts that have featured image
								);

								$format = $section['latest_by_format'][0]['post_format'] || 'standard';
								if ( 'standard' == $format ) {
									$args[ 'tax_query' ] = array(
										array(
											'taxonomy' => 'post_format',
											'field' => 'slug',
											'terms' =>  array( 'post-format-video', 'post-format-gallery', 'post-format-audio' ),
											'operator' => 'NOT IN',
										)
									);
								} else {
									$args[ 'tax_query' ] = array(
										array(
											'taxonomy' => 'post_format',
											'field' => 'slug',
											'terms' =>  array( 'post-format-' . $format ),
										)
									);
								}

								$the_query = new WP_Query( $args );

								echo '<div class="vwpc-section-latest_format">';
								acf_vwpc_render_post_section( $section['latest_by_format'][0], $the_query );
								echo '</div>';
								break;

							case '3 Sidebars':
								$sidebar_1 = $section['3_sidebars'][0]['sidebar_1'];
								$sidebar_2 = $section['3_sidebars'][0]['sidebar_2'];
								$sidebar_3 = $section['3_sidebars'][0]['sidebar_3'];

								echo '<div class="vwpc-section-3_sidebars">';

								// Sidebar 1
								echo '<div class="col-sm-4">';
								echo '<hr class="section-hr">';
								dynamic_sidebar( $sidebar_1 );
								echo '</div>';

								// Sidebar 2
								echo '<div class="col-sm-4">';
								echo '<hr class="section-hr">';
								dynamic_sidebar( $sidebar_2 );
								echo '</div>';

								// Sidebar 3
								echo '<div class="col-sm-4">';
								echo '<hr class="section-hr">';
								dynamic_sidebar( $sidebar_3 );
								echo '</div>';

								echo '</div>';
								break;

							case 'Custom Content':
								$title = $section['custom_content'][0]['title'];
								$super_title = $section['custom_content'][0]['super-title'];
								$sub_title = $section['custom_content'][0]['subtitle'];
								$sidebar = $section['custom_content'][0]['sidebar'];

								echo '<div class="vwpc-section-custom_content">';

								if ( '0' == $sidebar ) :
								echo '<div class="col-sm-12">';
								else :
								echo '<div class="col-sm-7 col-md-8">';
								endif;

								echo '<hr class="section-hr">';

								acf_vwpc_render_section_title_custom( $title, $super_title, $sub_title );

								echo apply_filters( 'the_content', $section['custom_content'][0]['content'] );
								echo '</div>';

								if ( '0' != $sidebar ) : ?>
									<div class="col-sm-5 col-md-4">
										<aside class="sidebar-wrapper">
											<div class="sidebar-inner">
												<hr class="section-hr">
												<?php dynamic_sidebar( $sidebar ); ?>
											</div>
										</aside>
									</div>
								<?php
								endif;

								echo '</div>';
								break;
						}

						echo '</div>';

					}

				}
			?>
			
		</div>
		
		<div class="col-sm-4 col-md-3">
			
			<?php include('asiabars-sidebar.php'); ?>
			
		</div>
	</div>
</div>

<?php endif; ?>

<?php get_footer(); ?>