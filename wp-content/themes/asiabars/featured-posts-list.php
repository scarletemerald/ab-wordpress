<?php 
/* 
Plugin Name:Recent Review Articles
Author: pointstar
Author URI: http://point-star.com/
*/




// Main function to diplay on front end

function featuredpostsList($before = '<li>', $after = '</li>') {
global $post, $wpdb, $posts_settings;
// posts_id from database
$posts_id = $posts_settings['posts_id'];

 if($posts_id) {
 
	$posts_idarray = explode(',',$posts_id);
	
		foreach ($posts_idarray as $list){
			$post = new WP_Query('p='.$list.'');
			$post->the_post();
			$post_title = stripslashes($post->post_title);
			$permalink = get_permalink($post->ID);
			echo $before . '<a href="' . $permalink . '" rel="bookmark" title="Permanent Link: ' . $post_title . '">' . $post_title . '</a>'. $after;

  }	
  } else {
		echo $before ."None found". $after;
	}
}

$data = array(
				'posts_id' 			=> ''
							);
$ol_flash = '';

 $posts_settings = get_option('posts_settings');

// ADMIN PANLE SEETTING
function posts_add_pages() {
    // Add new menu in Setting or Options tab:
    add_options_page('Recent Review Articles', 'Recent Review Articles', 8, 'postsoptions', 'posts_options_page');
}


/* Define Constants and variables*/
define('PLUGIN_URI', get_option('siteurl').'/wp-content/plugins/');

/* Functions */

function posts_options_page() {
global $ol_flash, $posts_settings, $_POST, $wp_rewrite;
if (isset($_POST['posts_id'])) { 
	$posts_settings['posts_id'] = $_POST['posts_id'];
	update_option('posts_settings',$posts_settings);
	$ol_flash = "Your  List has been saved.";
		}

if ($ol_flash != '') echo '<div id="message"class="updated fade"><p>' . $ol_flash . '</p></div>';

echo '<div class="wrap">';
		echo '<h2>Add Posts ID to Recent Review Articles</h2>';
		echo '<table class="optiontable form-table"><form action="" method="post">
		
		<tr><td><strong>Post ID :</strong></td><td><input type="text" name="posts_id" value="' . htmlentities($posts_settings['posts_id']) . '" size="50%" /></td></tr>
		<tr><td colspan="2"><strong>SAN Hint: To Add Multiple Post IDs use " , " for exmple : " 1, 2, 3" </strong></td></tr>
		</table>';
				
echo '<Div class="submit"><input type="submit" value="Save your list" /></div>';

}

add_action('admin_menu', 'posts_add_pages');

?>