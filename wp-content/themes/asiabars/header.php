<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<!-- css + javascript -->
	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.min.js"></script>
			<![endif]-->

			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-16608760-2', 'asia-bars.com');
				ga('send', 'pageview');

			</script>
		</head>
		<body id="top" <?php body_class(); ?>>

			<nav id="mobile-nav-wrapper" role="navigation"></nav>
			<div id="off-canvas-body-inner">

				<!-- Top Bar -->
				<div id="top-bar" class="top-bar">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">

								<a id="open-mobile-nav" href="#mobile-nav" title="<?php esc_attr_e( 'Search', 'envirra' ); ?>"><i class="icon-entypo-menu"></i></a>
								
								<nav id="top-nav-wrapper">
									<?php
									if ( has_nav_menu('top_navigation' ) ) {
										wp_nav_menu( array(
											'theme_location' => 'top_navigation',
											'container' => false,
											'menu_class' => 'top-nav list-unstyled clearfix',
											'link_before' => '<span>',
											'link_after' => '</span>',
											'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth' => 2,
											'walker' => new vw_text_menu_walker()
											) );
									}
									?>
								</nav>
								
							</div>
						</div>
					</div>
				</div>
				<!-- End Top Bar -->
				
				<!-- Main Bar -->
				<?php $header_layout = vw_get_option( 'header_layout', 'left-logo' ); ?>
				<header class="main-bar <?php echo 'header-layout-'.esc_attr( $header_layout ); ?>">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div id="logo-m" class="visible-xs visible-sm visible-md">

									<a href="<?php echo esc_url( home_url() ); ?>/">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/asiabars-logo-stacked-sm.png" alt="asia bars & restaurants">
									</a>
								</div>
								
								<div id="logo" style="width:35%;" class="hidden-xs hidden-sm hidden-md" >
									<div class="logogram">
										<a href="<?php echo esc_url( home_url() ); ?>/">
											<?php if( vw_get_option( 'logo_url' ) != '' ) : ?>
												<?php if( vw_get_option( 'logo_2x_url' ) != '' ) : ?>
													<img src="<?php echo esc_url( vw_get_option( 'logo_2x_url' ) ); ?>" width="<?php echo esc_attr( vw_get_option( 'logo_2x_width' ) ); ?>" height="<?php echo esc_attr( vw_get_option( 'logo_2x_height' ) ); ?>" alt="<?php bloginfo('name'); ?>" class="logo-retina" />
												<?php endif; ?>
												<img src="<?php echo esc_url( vw_get_option( 'logo_url' ) ); ?>" alt="<?php bloginfo('name'); ?>" class="logo-original" />
											<?php else : ?>
												<h1 id="site-title" class="title title-large"><?php bloginfo( 'name' ); ?></h1>
											<?php endif; ?>		
											<?php if ( get_bloginfo( 'description' ) ): ?>
												<h2 id="site-tagline" class="subtitle"><?php bloginfo( 'description' ) ?></h2>
											<?php endif; ?>					
										</a>
									</div>
									
								</div>
								<div class="logo-tagline" style="height:90;width:728;">
								<?php if( is_home() || is_front_page() ) { ?>
									<script language='JavaScript' type='text/javascript'>var randomstr = new String (Math.random());randomstr = randomstr.substring(1,8);var SID_Mobile = 158658875864;var SID_Tablet = 121045805857;var SID_PC = 121045805857;var pixelSID = SID_PC; var isMobile = 0; var hideIfMobile = 0; if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Mobile;} else if(/iPad|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Tablet;} else {isMobile=1;pixelSID=SID_PC;}if ((isMobile==1) && (hideIfMobile==1)) {} else {document.write ("<" + "script language='JavaScript' type='text/javascript' src='" + "http://pixel-my.pixelinteractivemedia.com/adj.php?ts=" + randomstr + "&amp;sid=" + pixelSID + "'><" + "/script>");}</script>
								<?php } else { ?>
									<!-- ROS Script -->
									<script language='JavaScript' type='text/javascript'>var randomstr = new String (Math.random());randomstr = randomstr.substring(1,8);var SID_Mobile = 837226885866;var SID_Tablet = 506143155858;var SID_PC = 506143155858;var pixelSID = SID_PC; var isMobile = 0; var hideIfMobile = 0; if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Mobile;} else if(/iPad|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Tablet;} else {isMobile=1;pixelSID=SID_PC;}if ((isMobile==1) && (hideIfMobile==1)) {} else {document.write ("<" + "script language='JavaScript' type='text/javascript' src='" + "http://pixel-my.pixelinteractivemedia.com/adj.php?ts=" + randomstr + "&amp;sid=" + pixelSID + "'><" + "/script>");}</script>
								<?php } ?>
								</div>
								<div class="search-top" style="margin-top: 100px; padding-right: 15px;"> 
									<form action="<?php echo home_url(); ?>/" id="searchform" class="searchform pull-right" method="get">
										<input type="text" id="s" name="s" value="<?php _e('Search', 'envirra') ?>" onfocus="if(this.value=='<?php _e('Search', 'envirra') ?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e('Search', 'envirra') ?>';" autocomplete="off" />
										<button class="search-button"><i class="icon-entypo-search"></i></button>
									</form>
								</div>
								<?php if( vw_get_option( 'header_ads_code' ) != '' ) : ?>
									<div class="header-ads">
										<?php echo vw_get_option( 'header_ads_code' ) ; ?>
									</div>
								<?php endif; ?>		
								
								
								
							</div>
						</div>
					</div>
				</header>
				<!-- End Main Bar -->

				<!-- Main Navigation Bar -->
				<div class="main-nav-bar <?php echo 'header-layout-'.esc_attr( $header_layout ); ?>">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<?php
								if ( has_nav_menu('main_navigation' ) ) {
									wp_nav_menu( array(
										'theme_location' => 'main_navigation',
										'container' => false,
										'menu_class' => 'main-nav list-unstyled',
										'link_before' => '<span>',
										'link_after' => '</span>',
										'items_wrap' => '<nav id="main-nav-wrapper"><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
										'depth' => 2,
										'walker' => new vw_main_menu_walker()
										) );
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<!-- End Main Navigation Bar -->

				<!-- pixel add start -->
				<div style="width:100%;height:300px;">
					<?php if( is_home() || is_front_page() ) { ?>
						<script language='JavaScript' type='text/javascript'>var randomstr = new String (Math.random());randomstr = randomstr.substring(1,8);var SID_Mobile = 919422395871;var SID_Tablet = 746746285861;var SID_PC = 746746285861;var pixelSID = SID_PC; var isMobile = 0; var hideIfMobile = 0; if(/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Mobile;} else if(/iPad|IEMobile|Opera Mini/i.test(navigator.userAgent)) {isMobile=0;pixelSID=SID_Tablet;} else {isMobile=1;pixelSID=SID_PC;}if ((isMobile==1) && (hideIfMobile==1)) {} else {document.write ("<" + "script language='JavaScript' type='text/javascript' src='" + "http://pixel-my.pixelinteractivemedia.com/adj.php?ts=" + randomstr + "&amp;sid=" + pixelSID + "'><" + "/script>");}</script>
					<?php } ?>
				</div>
				<!-- pixel add end -->