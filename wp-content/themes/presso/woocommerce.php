<?php get_header(); ?>

<div id="page-wrapper" class="container">
	<div class="row">

		<div id="page-content" class="col-sm-12">

			<?php if ( ! is_shop() ) woocommerce_get_template( 'shop/breadcrumb.php', apply_filters( 'woocommerce_breadcrumb_defaults', array() ) ); ?>

			<?php /* Page title */ ?>
			<?php if ( ! is_singular( 'product' ) ) : ?>
				<h1 class="page-title title title-large"><?php woocommerce_page_title(); ?></h1>
				<?php 
				$page_subtitle = trim( get_post_meta( get_option('woocommerce_shop_page_id'), 'vw_page_subtitle', true ) );

				if ( is_shop() && ! empty( $page_subtitle ) ) : ?>
				<h2 class="subtitle"><?php echo $page_subtitle; ?></h2>
				<?php endif; ?>
				<hr>
			<?php endif; ?>

			<?php woocommerce_content(); ?>

		</div>

	</div>
</div>

<?php get_footer(); ?>