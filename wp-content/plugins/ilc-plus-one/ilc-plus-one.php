<?php
/**
 * @package ILC_Plus_One
 * @version 1.0.0
 * Plugin Name: ILC +1
 * Plugin URI: http://ilovecolors.com.ar/
 * Adds Google's +1 button to your post or your sidebar.
 * Author: Elio Rivero
 * Version: 1.0.0
 * Author URI: http://ilovecolors.com.ar
*/

/**
 * Get slider global settings.
 * @since 1.0.0
 */
$ilc_plus_one_settings = get_option('stored_ilc_plus_one');


/**
 * ILC Plus One Widget Class for Categories
 * @since 1.0.0
 * @extends WP_Widget
 */
class ILC_Plus_One extends WP_Widget {

	function ILC_Plus_One() {
		$widget_ops = array( 'classname' => 'ilc-plus-one', 'description' => __('Displays a tabbed panel with recent posts from three categories.', 'ilc') );
		$control_ops = array( 'id_base' => 'ilc-plus-one' );
		$this->WP_Widget( 'ilc-plus-one', __('ILC +1', 'ilc'), $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		global $ilc_plus_one_settings;
		
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );
		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title;
		
		$p1 = '<g:plusone';
		if($instance['size'] != "")
			$p1 .= ' size="' . $instance['size'] . '"';
		if(!$instance['count'])
			$p1 .= ' count="false"';
		if(!$ilc_plus_one_settings['track'])
			$p1 .= ' callback="plusone_vote"';
		$p1 .= ' href="' . site_url() . '"></g:plusone>';
		echo $p1 . site_url();
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['size'] = strip_tags( $new_instance['size'] );
		$instance['count'] = strip_tags( $new_instance['count'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array(
			'title' => __('+1 this site!', 'ilc'),
			'size' => "",
			'count' => true
			);
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ilc'); ?>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $instance['title']; ?>" class="widefat" /></label>
			<small><?php _e('Enter your title or leave blank to hide .') ?></small>
		</p>
	
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>">
			<input id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" type="checkbox" <?php if ( $instance['count'] ) echo 'checked="checked"'; ?> value="true" /> 
			<?php _e( 'Include count.', 'ilc' ); ?></label>		
		</p>
		
		<p>
			<table class="ilc-plus-one-sizes">
				<tr>
					<td>
						<?php echo '<img src="' . plugins_url( 'standard.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="standard" name="<?php echo $this->get_field_name( 'size' ); ?>" type="radio" <?php if ( $instance['size'] == "") echo 'checked="checked"'; ?> value="" />
						<label for="standard"><?php _e( 'Standard.', 'ilc' ); ?></label>
					</td>
		
					<td>
						<?php echo '<img src="' . plugins_url( 'medium.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="medium" name="<?php echo $this->get_field_name( 'size' ); ?>" type="radio" <?php if ( $instance['size'] == "medium") echo 'checked="checked"'; ?> value="medium" /> 
						<label for="medium"><?php _e( 'Medium.', 'ilc' ); ?></label>
					</td>
				</tr><tr>
					<td>
						<?php echo '<img src="' . plugins_url( 'small.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="small" name="<?php echo $this->get_field_name( 'size' ); ?>" type="radio" <?php if ( $instance['size'] == "small") echo 'checked="checked"'; ?> value="small" /> 
						<label for="small"><?php _e( 'Small.', 'ilc' ); ?></label>
					</td>
					
					<td>
						<?php echo '<img src="' . plugins_url( 'tall.gif' , __FILE__ ) . '" > '; ?><br/>
						<input id="tall" name="<?php echo $this->get_field_name( 'size' ); ?>" type="radio" <?php if ( $instance['size'] == "tall") echo 'checked="checked"'; ?> value="tall" /> 
						<label for="tall"><?php _e( 'Tall.', 'ilc' ); ?></label>
					</td>
				</tr>
			</table>
			<style>
				.ilc-plus-one-sizes td { padding:0 25px 25px 0; }
			</style>
		</p>
		
		<?php
		
	}
}

function  ilc_plus_one_output($content){
	global $ilc_plus_one_settings;
	
	if($ilc_plus_one_settings['place'] == 'after')
		return $content . ilc_plus_one_get();
	else
		return ilc_plus_one_get() . $content;
}

function ilc_plus_one_get(){
	global $ilc_plus_one_settings, $post;

	$p1 = '<div class="ilc-plus-one"';
	if($ilc_plus_one_settings['float'] != "none")
		$p1 .= 'style="float: ' . $ilc_plus_one_settings['float'] . ';"';
	$p1 .= '>';
	$p1 .= '<g:plusone';
	
	if($ilc_plus_one_settings['size'] != "")
		$p1 .= ' size="' . $ilc_plus_one_settings['size'] . '"';
	if(!$ilc_plus_one_settings['count'])
		$p1 .= ' count="false"';
	if(!$ilc_plus_one_settings['track'])
		$p1 .= ' callback="plusone_vote"';
	$p1 .= ' href="' . get_permalink($post->id) . '"></g:plusone></div>';
	
	return $p1;
}

function ilc_plus_one_enqueue(){
	global $ilc_plus_one_settings;
	echo '<script type="text/javascript" src="https://apis.google.com/js/plusone.js">';
		if($ilc_plus_one_settings['lang'] != "")
			echo '{lang: \'' . $ilc_plus_one_settings['lang'] . '\'}';
	echo '</script>';
	echo '<script type="text/javascript"> function plusone_vote( obj ) { _gaq.push([\'_trackEvent\',\'plusone\',obj.state]); } </script>';
}

add_action( 'widgets_init', create_function("", "register_widget('ILC_Plus_One');") );
add_action( 'wp_footer', 'ilc_plus_one_enqueue');
add_filter( 'the_content', 'ilc_plus_one_output');

if(is_admin()) require_once( 'ilc-admin.php');


?>