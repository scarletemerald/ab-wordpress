<tr id="nivo_skin_theme">
			
	<td class="label">
		<label>
			<?php _e( 'Theme', SLIDER_PLUGIN_PREFIX ); ?>
		</label>
		<p class="description">
					<?php _e( 'Choose the Nivo Slider Skin theme', SLIDER_PLUGIN_PREFIX ); ?>
		</p>
	</td>
	<td>
		<select name="slider_options[nivo_slider_theme]">
			<option value="theme-default"
			<?php selected( "theme-default", $active_theme ) ?>>Default</option>
			<option value="theme-bar"
			<?php selected( "theme-bar", $active_theme ) ?>>Bar</option>
			<option value="theme-light"
			<?php selected( "theme-light", $active_theme ) ?>>Light</option>
			<option value="theme-dark"
			<?php selected( "theme-dark", $active_theme ) ?>>Dark</option>
			<option value="theme-pascal"
			<?php selected( "theme-pascal", $active_theme ) ?>>Pascal</option>
		</select>
	</td>
</tr>