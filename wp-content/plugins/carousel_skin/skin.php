<?php
/**
Plugin Name: Carousel Skin for WP slider
Description: Simple, lightweight and responsive carousel skin for WordPress slider plugin
Author: Muneeb
Version: 0.1

Copyright(c) 2012 Muneeb ur Rehman http://muneeb.me
**/

function m_ssp_register_carousel_skin( $skins ) {

	$path = plugin_dir_path( __FILE__ );

	$skins[] = array(
			'name' => __( 'Simple Carousel Skin' ),
			'path' => $path,
			'description' => 'Simple, lightweight and responsive carousel skin'
		);

	return $skins;

}

//Add this skin to the slider skins option array
add_filter( 'ssp_skins_array', 'm_ssp_register_carousel_skin' );

?>